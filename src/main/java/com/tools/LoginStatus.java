package com.tools;

import com.bean.User;
import com.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LoginStatus {
    HttpServletRequest request;
    HttpSession session;
    public LoginStatus(HttpServletRequest request){
        this.request=request;
        this.session = request.getSession();
    }

    public  void login( String phone )
    {
        session.setAttribute("phone", phone);
        session.setAttribute("time", System.currentTimeMillis());
    }

    public User checkLogin()
    {
        Object attribute = session.getAttribute("phone");
        if (attribute !=null )
        {
            UserService userService = new UserService();
            try {
                return userService.findUserByPhone( (String)attribute);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
