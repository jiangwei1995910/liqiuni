package com.controller;


import com.bean.Activity;
import com.bean.User;
import com.service.ActivityService;
import com.service.ThingServier;
import com.service.UserService;
import com.tools.LoginStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/")
public class IndexController {


    @RequestMapping("/")
    public ModelAndView index() throws Exception {
        ModelAndView modelAndView=new ModelAndView("/view/index.jsp");
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        LoginStatus loginStatus = new LoginStatus(request);
        User u= loginStatus.checkLogin();
        modelAndView.addObject("user",u);
        ActivityService activityService = new ActivityService();
        List<Activity> activities = activityService.findAllActivity();
        UserService userService=new UserService();
        List<User> allUsers = userService.findAllUsers();
        ThingServier thingServier=new ThingServier();
        double money=activityService.countMoney();
        if (activities.size() >=10 )
            modelAndView.addObject("activities",activities.subList(0,9));
        else
            modelAndView.addObject("activities",activities);
        modelAndView.addObject("money",money);
        modelAndView.addObject("totalPeople",allUsers.size());
        modelAndView.addObject("thingCount",thingServier.count());
        modelAndView.addObject("finishCount",activityService.finishCount());
        return modelAndView;
    }

}
