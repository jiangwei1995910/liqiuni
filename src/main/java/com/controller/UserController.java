package com.controller;
import com.bean.Activity;
import com.bean.User;
import com.service.ActivityService;
import com.service.DonateService;
import com.service.UserService;
import com.tools.LoginStatus;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController  {
    @RequestMapping(value = "add",method=RequestMethod.POST)
    public String add(User user) throws IOException {
        HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
        UserService userService = new UserService();
        try {
            userService.insertUser( user );
        } catch (Exception e) {
            System.out.println(e);
            return "<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\">"+
                    "error! phone repeat  <script>\n" +
                    "    var t = 5;\n" +
                    "    function countDown(){\n" +
                    "        t--;\n" +
                    "        if (t<=0) {\n" +
                    "            location.href=\"/user\";\n" +
                    "            clearInterval(inter);\n" +
                    "        };\n" +
                    "    }\n" +
                    "    var inter = setInterval(\"countDown()\",1000);\n" +
                    "    window.onload=countDown;\n" +
                    "\n" +
                    "</script>";
        }
        return "<script> window.location.href='/user';  </script>";
    }

    @RequestMapping(value = "login",method = RequestMethod.POST)
    public ModelAndView login(User user ){
        UserService userService = new  UserService();
        try {
            int id =userService.verify(user);
            if (id >0)
            {
                HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
                HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
                LoginStatus loginStatus = new LoginStatus(request);
                loginStatus.login(user.getPhone());
                response.sendRedirect("/");
                return null;
            }else {
                ModelAndView modelAndView=new ModelAndView("/view/info.jsp");
                modelAndView.addObject("title","登录失败");
                return modelAndView;
            }
        } catch (IOException e) {
            e.printStackTrace();
            ModelAndView modelAndView=new ModelAndView("/view/info.jsp");
            modelAndView.addObject("title",new String("登录失败"));
            return modelAndView;
        }
    }

    @RequestMapping("")
    public ModelAndView  index()
    {
        ModelAndView modelAndView=new ModelAndView("/view/login.jsp");
        return modelAndView;
    }

    @RequestMapping("signup")
    public ModelAndView signup()
    {
        ModelAndView modelAndView=new ModelAndView("/view/signup.jsp");
        return modelAndView;
    }

    @RequestMapping("home")
    public ModelAndView home() throws IOException {
        DonateService donateService=new DonateService();
        HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
        ModelAndView modelAndView=new ModelAndView("/view/userHome.jsp");
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        LoginStatus loginStatus = new LoginStatus(request);
        User u= loginStatus.checkLogin();
        if (u==null){
            response.sendRedirect("/");
            return null;
        }
        modelAndView.addObject("user",u);
        ActivityService activityService = new ActivityService();
        List<Activity> activities = activityService.findActivityByPhone(u.getPhone());
        System.out.println(activities);
        modelAndView.addObject("activities",activities);
        modelAndView.addObject("total",donateService.total(u.getPhone()));
        modelAndView.addObject("count",donateService.count(u.getPhone()));
        return modelAndView;
    }


    @RequestMapping("update")
    public User update(User user) throws IOException {
        System.out.println(user);
        UserService userService = new UserService();
        userService.updateInfo(user);
        return user;
    }


    @RequestMapping("loginout")
    public String loginOut(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        request.getSession().removeAttribute("phone");
        request.getSession().removeAttribute("time");
        return "<script> window.location.href='/';  </script>";
    }

}
