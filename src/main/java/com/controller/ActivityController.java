package com.controller;


import com.bean.Activity;
import com.bean.Donate;
import com.bean.Thing;
import com.bean.User;
import com.service.ActivityService;
import com.service.DonateService;
import com.service.ThingServier;
import com.service.UserService;
import com.tools.LoginStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/activity")
public class ActivityController {

    @RequestMapping("/")
    public ModelAndView index() throws Exception {
        ModelAndView modelAndView = new ModelAndView("/view/activity.jsp");
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        LoginStatus loginStatus = new LoginStatus(request);
        ActivityService activityService=new ActivityService();
        User u = loginStatus.checkLogin();


        List<Activity> activities = activityService.findAllActivity();
        modelAndView.addObject("user", u);
        modelAndView.addObject("activities",activities);
        return modelAndView;
    }

    @RequestMapping("new")
    public ModelAndView newActivity(Activity activity) throws Exception {
        System.out.println(activity);
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        ModelAndView modelAndView = new ModelAndView("/view/newActivity.jsp");
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        LoginStatus loginStatus = new LoginStatus(request);
        User u = loginStatus.checkLogin();
        if (u == null) {
            response.sendRedirect("/");
        }
        if (activity.getType() != null) {
            ActivityService acticityService = new ActivityService();
            activity.setPhone(u.getPhone());
            acticityService.insertUser(activity);
            response.sendRedirect("/user/home");
            System.out.println(activity);
        }

        return modelAndView;
    }

    @RequestMapping("content")
    public ModelAndView activityContent( int id ) throws Exception {
        ActivityService activityService = new ActivityService();
        Activity activity = activityService.findActivityById(id);
        UserService userService = new UserService();

        ModelAndView modelAndView = new ModelAndView("/view/activityContent.jsp");
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        LoginStatus loginStatus = new LoginStatus(request);
        User u = loginStatus.checkLogin();
        DonateService donateService = new DonateService();
        if (u != null)
        {

            List<Donate> donates = donateService.findByPhone(u.getPhone());
            List<Donate> donates2=new ArrayList<Donate>() ;
            for (int i=0;i<donates.size();i++){
                if (donates.get(i).getActid()  == id    ){
                    donates2.add(donates.get(i));
                }
            }


            modelAndView.addObject("donates",donates2);
        }

        List<Donate> getDonates = donateService.findByActid(String.valueOf(id));
        modelAndView.addObject("getDonates",getDonates);

        modelAndView.addObject("activity",activity);
        User user = userService.findUserByPhone(activity.getPhone());
        modelAndView.addObject("user",user);
        return modelAndView;
    }


    @RequestMapping("donate")
    public String donate(int actID , String content,String method  ,String phone ) throws Exception {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        LoginStatus loginStatus = new LoginStatus(request);
        User user = loginStatus.checkLogin();
        if (user ==null )
        {
            return "301" ;
        }

        String contentUtf8="";
        try {
            contentUtf8 = new String(content.getBytes("ISO-8859-1"), "utf-8");
        }catch (Exception e){

        }
        DonateService donateService=new DonateService();
        ActivityService activityService=new ActivityService();
        Activity activityById = activityService.findActivityById(actID);
        donateService.add(new Donate(method,contentUtf8,phone ,actID ));
        if (method.equals("2")){
            activityService.updateMoney(activityById, Integer.valueOf(content)   );
        }
        if (method.equals("1")){
            ThingServier thingServier=new ThingServier();
            thingServier.insertThing(new Thing(contentUtf8 , phone));
        }



        return "200";

    }


}
