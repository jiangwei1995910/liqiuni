package com.service;

import com.bean.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class UserService {
    private String resource="mybits/mybits.xml";

    public User findUserById(int id) throws Exception {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        User user = session.selectOne("user.findUserById",id); //参数一：namespace.id
        session.close();
        return user;
    }

    public User findUserByPhone(String phone) throws Exception {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        User user = session.selectOne("user.findUserByPhone",phone); //参数一：namespace.id
        session.close();
        return user;
    }//通过号码查询用户

    public List<User> findAllUsers() throws Exception {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        List<User> users = session.selectList("user.findUserAll");
        session.close();
        return users;
    }

    public void insertUser(User user) throws Exception {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        session.insert("user.insertUser", user);
        session.commit();   //增删改，一定一定要加上commit操作
        session.close();
    }//插入用户数据

    public void deleteUserById(int id) throws Exception {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        session.delete("user.deleteUserById", id);
        session.commit();   //增删改，一定一定要加上commit操作
        session.close();
    }

    public void updateUserPassword(User user) throws Exception {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        session.update("user.updateUserPassword", user);
        session.commit();
        session.close();
    }


    public int verify(User user ) throws IOException {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        int id=0;
        try{
            id = session.selectOne("user.verify",user);
        }catch (Exception e){
            id =0;
        }
        session.close();
        return id;
    }//验证用户名密码


    public void updateInfo(User user) throws IOException {
        System.out.println(user);
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        session.update("user.updateInfo", user);
        session.commit();
        session.close();
    }//更新用户数据

}
