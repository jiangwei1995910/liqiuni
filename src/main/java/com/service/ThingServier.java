package com.service;

import com.bean.Activity;
import com.bean.Thing;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class ThingServier {
    private String resource="mybits/mybits.xml";

    public void insertThing(Thing thing) throws Exception {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        session.insert("thing.addThing", thing);
        session.commit();
        session.close();
    }


    public int count() throws IOException {

        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        int c=0;
        try{
            c = session.selectOne("thing.count"); //参数一：namespace.id
        }catch (Exception e){
            System.out.println(e);
        }

        session.close();
        return c;

    }
}
