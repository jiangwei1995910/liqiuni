package com.service;

import com.bean.Activity;
import com.bean.Donate;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class DonateService {
    private String resource = "mybits/mybits.xml";

    public void add(Donate donate) throws IOException {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        session.insert("donate.add", donate);
        session.commit();   //增删改，一定一定要加上commit操作
        session.close();
    }//用户参与捐赠数据：目前情况表

    public double total(String phone) throws IOException {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        double total = 0;

        try {
            total = session.selectOne("donate.total", phone);
        } catch (Exception e) {
            e.printStackTrace();
        }

        session.commit();   //增删改，一定一定要加上commit操作
        session.close();
        return total;
    }//个人捐赠总数


    public int count(String phone) throws IOException {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        int count = session.selectOne("donate.count", phone);
        session.commit();   //增删改，一定一定要加上commit操作
        session.close();
        return count;
    }//参与活动统计

    public List<Donate> findByPhone(String phone) throws IOException {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        List<Donate> donates = session.selectList("donate.selectByPhone", phone);
        session.close();
        return donates;
    }

    public List<Donate> findByActid(String actid) throws IOException {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        List<Donate> donates = session.selectList("donate.selectByActid", actid);
        session.close();
        return donates;
    }
}
