package com.service;

import com.bean.Activity;
import com.bean.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ActivityService {

    private String resource="mybits/mybits.xml";
    public Activity findActivityById(int id) throws Exception {
        InputStream inputStream = Resources.getResourceAsStream(resource);   //打开配置文件
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream); //为mybits指定配置文件
        SqlSession session = factory.openSession();   //打开数据库连接
        Activity activity = session.selectOne("activity.findActivityById",id); //指定执行哪一条SQL
        session.close();  //关闭数据库连接
        return activity;
    }

    public List<Activity> findAllActivity() throws Exception {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        List<Activity> activities = session.selectList("activity.findActivityAll");
        session.close();
        return activities;
    }


    public void insertUser(Activity activity) throws Exception {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        session.insert("activity.insertActivity", activity);
        session.commit();
        session.close();//根据用户插入活动
    }


    public void deleteActivityById(int id) throws Exception {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        session.delete("activity.deleteActivityById", id);
        session.commit();
        session.close();
    }

    public void updateInfo(Activity activity) throws IOException {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        session.update("activity.updateActivity", activity);
        session.commit();
        session.close();//插入活动后跟更新活动
    }

    public List<Activity> findActivityByPhone(String phone) throws IOException {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        List<Activity> activities = session.selectList("activity.findActivityAllByPhone" ,phone );
        session.close();
        return activities;
    }


    public double countMoney() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        double money=0;
        try {
            money= session.selectOne("activity.findActivityMoney"); //参数一：namespace.id
        }catch (Exception e){
        }//z金额总数

        session.close();
        return money;
    }



    public int finishCount() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        int c=0;
        try {
            c= session.selectOne("activity.finishCount"); //参数一：namespace.id
        }catch (Exception e){
        }
        session.close();
        return c;//完成任务
    }


    public void updateMoney(  Activity activity,double money  ) throws IOException {
        double money1 = activity.getMoney();
        activity.setMoney(money+money1);
        System.out.println(activity);
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = factory.openSession();
        session.update("activity.updateActivity",activity);
        session.commit();
        session.close();//更新金额
    }


}
