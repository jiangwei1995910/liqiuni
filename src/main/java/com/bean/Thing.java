package com.bean;

public class Thing {
    private Integer id;
    private String thingname;
    private String phone;

    public String getThingname() {
        return thingname;
    }

    public void setThingname(String thingname) {
        this.thingname = thingname;
    }

    public Thing(String name, String phone) {
        this.thingname = name;
        this.phone = phone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
