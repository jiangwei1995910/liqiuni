package com.bean;



public class Activity {
    private int id;
    private String type;
    private String address;
    private String starttimes;
    private String endtimes;
    private String desc;
    private int intentmoney;
    private String needthing;
    private int peoplenum;
    private String theme;
    private double money;

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    private int status;
    private String content;
    private String img;
    private String phone;

    public String getStarttimes() {
        return starttimes;
    }

    public void setStarttimes(String starttimes) {
        this.starttimes = starttimes;
    }

    public String getEndtimes() {
        return endtimes;
    }

    public void setEndtimes(String endtimes) {
        this.endtimes = endtimes;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id=" + id +
                ", money=" + money +
                ", type='" + type + '\'' +
                ", address='" + address + '\'' +
                ", starttimes='" + starttimes + '\'' +
                ", endtimes='" + endtimes + '\'' +
                ", desc='" + desc + '\'' +
                ", intentmoney=" + intentmoney +
                ", needthing='" + needthing + '\'' +
                ", peoplenum=" + peoplenum +
                ", theme='" + theme + '\'' +

                ", status=" + status +
                ", content='" + content + '\'' +
                ", img='" + img + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getIntentmoney() {
        return intentmoney;
    }

    public void setIntentmoney(int intentmoney) {
        this.intentmoney = intentmoney;
    }

    public String getNeedthing() {
        return needthing;
    }

    public void setNeedthing(String needthing) {
        this.needthing = needthing;
    }

    public int getPeoplenum() {
        return peoplenum;
    }

    public void setPeoplenum(int peoplenum) {
        this.peoplenum = peoplenum;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
}
