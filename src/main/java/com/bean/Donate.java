package com.bean;

public class Donate {
    private Integer id;
    private String type;
    private String content;
    private String phone;
    private Integer actid;
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getActid() {
        return actid;
    }

    public void setActid(Integer actid) {
        this.actid = actid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Donate(String type, String content, String phone, Integer actid) {
        this.type = type;
        this.content = content;
        this.phone = phone;
        this.actid = actid;
    }

    public Donate(Integer id, String type, String content, String phone, Integer actid, Integer status) {
        this.id = id;
        this.type = type;
        this.content = content;
        this.phone = phone;
        this.actid = actid;
        this.status = status;
    }


    public Donate(String type, String content, String phone, Integer actid, Integer status) {
        this.type = type;
        this.content = content;
        this.phone = phone;
        this.actid = actid;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Donate{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
