<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>首页</title>
    <link href="/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="/css/commen.css" type="text/css" rel="stylesheet"/>
    <link href="/css/index.css" type="text/css" rel="stylesheet"/>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/vue.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row header">
        <div class="col-md-1 header-item">首页</div>
        <a href="/activity/"><div class="col-md-1 header-item">活动</div></a>
        <div class="col-md-1 header-item call">联系我们</div>
        <div class="col-md-1 header-item">搜索</div>
        <div class="col-md-1"></div>
        <c:if test="${user != null}">
            <a href="/user/home"><div class="col-md-2 float-right header-item">欢迎您，${user.phone}</div></a>
        </c:if>
        <c:if test="${user == null}">
            <a href="/user/"><div class="col-md-2 float-right header-item">登录</div></a>
        </c:if>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-2 info">
            <div class="float-left info-item">${thingCount}<br>捐赠</div>
            <div class="float-left info-item">${totalPeople}<br>志愿者</div>
            <div class="float-left info-item">${finishCount}<br>完成任务</div>
            <div class="float-left info-item">${money}<br>款项</div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <img width="100%" height="150px" src="/images/1.jpg">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div id="activity">
                <c:forEach items="${activities}" var="item" begin="0"    end="4"    step="1">
                    <a href="/activity/content?id=${item.id}">
                    <div class="act_item" style="background:#000000 url(${item.img}) repeat-x;  ">
                        <div class="desc">
                                ${item.theme}
                        </div>
                           <div class="times">
                                ${item.starttimes}-${item.endtimes}
                           </div>

                    </div>
                    </a>
                </c:forEach>
            </div>
        </div>
    </div>

 <div class="row">
        <div class="col-md-3">
            <img width="530px" height="396px" src="/images/2.jpg">
        </div>
       <div class="col-md-2 col-md-offset-4">

            <ul>
                <li><h4>成为志愿者</h4></li>
                <li><h4>捐物</h4></li>
                <li><h4>捐资</h4></li>

            </ul>
       </div>

</div>



<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">联系我们</h4>
            </div>
            <div class="modal-body">邮箱：839704462@QQ.COM</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<script>
    $(".call").click(function () {
        $("#myModal").modal();
    });
</script>


</body>
</html>
