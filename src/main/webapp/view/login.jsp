<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录</title>
    <link href="/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="/css/commen.css" type="text/css" rel="stylesheet"/>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

</head>
<body style="background-image:url(/images/5.jpg)">

<div class="container">
    <div class="height180"></div>
    <form action="/user/login" method="post">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 main">
            <div class="form-group">
                <label for="phone">手机号</label>
                <input id="phone" class="form-control" name="phone" placeholder="手机号">
            </div>
            <div class="form-group">
                <label for="password">密码</label>
                <input id="password" type="password" class="form-control" name="password" placeholder="密码">
            </div>
            <div class="form-group">
                <input class="form-control btn btn-primary login-btn" name="sub" type="submit" value="登录">
                <a href="/user/signup" class="form-control btn btn-default signup">注册 </a>
            </div>
        </div>
    </div>
    </form>


</div>
</body>
</html>
