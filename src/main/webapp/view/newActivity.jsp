<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset="utf-8">
    <title>发起活动</title>
    <link href="/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="/css/commen.css" type="text/css" rel="stylesheet"/>
    <link href="/css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet"/>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/vue.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/moment.min.js"></script>
    <script src="/js/bootstrap-datetimepicker.min.js"></script>
</head>
<body style="background-image:url(/images/5.jpg)">
<div class="container">
    <div class="height100"></div>

    <form action="" method="post">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>活动类型</label>
                    <%--<input type="text" name="type" class="form-control" required>--%>
                    <select name="type" class="form-control" id="type" onchange="change()">
                        <option value="志愿者">志愿者</option>
                        <option value="捐款">捐款</option>
                        <option value="捐物">捐物</option>
                    </select>


                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>地点</label>
                    <input type="text" name="address" class="form-control" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>时间</label>
                    <div>
                        <input id="starttime" name="starttimes" type="text" class="time form-control " required>
                        <input  id="endtime"  name="endtimes" type="text" class="time form-control " required>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>内容</label>
                    <input type="text" name="content" class="form-control" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>目标金额</label>
                    <input disabled id="intentmoney" type="text" name="intentmoney" class="form-control" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>所需物资</label>
                    <input disabled id="thing"  type="text" name="needthing" class="form-control" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>招募人数</label>
                    <input  id="peopleNum"  type="text" name="peoplenum" class="form-control" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>主题</label>
                    <input type="text" name="theme" class="form-control" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>描述</label>
                    <input type="text" name="desc" class="form-control" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>封面图片</label>
                    <input onchange="file(this.files)" type="file"  class="form-control" required>
                    <input type="hidden" name="img" id="img2">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2 float-right">
                <input type="submit" class="btn btn-primary"  value="确定" />
            </div>
        </div>
    </form>

</div>
</body>
<script>

    function file(file)
    {
        var reader = new FileReader();
        reader.readAsDataURL(file[0]);
        reader.onload = function (e) {
            var url = e.target.result;
            $("#img2").val(url);
        }
    }

    $("#starttime").datetimepicker({
        format: "YYYY-MM-DD"
    })
    $("#endtime").datetimepicker({
        format: "YYYY-MM-DD"
    })

    function change(){
        var type=$("#type").val();
        if (type == "志愿者"){
            $("input").removeAttr("disabled");
            $("#thing").attr("disabled",true);
            $("#intentmoney").attr("disabled",true);
        } else if(type=="捐款"){
            $("input").removeAttr("disabled");
            $("#peopleNum").attr("disabled",true);
            $("#thing").attr("disabled",true);
        }else if (type == "捐物"){
            $("input").removeAttr("disabled");
            $("#intentmoney").attr("disabled",true);
            $("#peopleNum").attr("disabled",true);
        }
    }



</script>

</html>
