<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>注册</title>
    <link href="/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="/css/commen.css" type="text/css" rel="stylesheet"/>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body style="background-image:url(/images/5.jpg)">

<div class="container">
    <div class="height100"></div>

    <div class="row">
        <form action="/user/add" method="post">
            <div class="col-md-6 col-md-offset-3">
                <div class="form-group">
                    <label for="phone">联系电话*：</label>
                    <input type="text" name="phone" class="form-control" id="phone" placeholder="phone">
                </div>
                <div class="form-group">
                    <label for="phone">密码*：</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="phone">
                </div>
                <div class="form-group">
                    <label for="password2">确认密码*：</label>
                    <input type="password"  class="form-control" id="password2" placeholder="phone">
                </div>
                <div class="form-group">
                    <label for="phone">姓名：</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="姓名">
                </div>
                <div class="form-group">
                    <label for="phone">昵称：</label>
                    <input type="text" name="nickname" class="form-control" id="nickname" placeholder="昵称">
                </div>
                <div class="form-group">
                    <label for="phone">性别：</label>
                    <select name="gender" class="form-control">
                        <option>男</option>
                        <option>女</option>
                    </select>


                </div>
                <div class="form-group">
                    <input type="button" id="sub" class="form-control btn btn-primary" value="确定">
                </div>
            </div>
        </form>

    </div>
</div>
</div>


</body>

<script>
    $("#sub").click(function () {
        var password=$("#password").val(),
            phone=$("#phone").val(),
            password2=$("#password2").val();
        if (password==password2  &&  password.length>0  ){
            if (phone.length>0){
                $("form").submit();
            }else {
                alert("请输入手机号");
            }
        } else {
            alert("两次密码不相同");
        }

    });
</script>

</html>
