<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>活动详情</title>
    <meta charset="utf-8">
    <link href="/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="/css/commen.css" type="text/css" rel="stylesheet"/>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/vue.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row header">
        <a href="/"> <div class="col-md-1 header-item">首页</div></a>
        <a href="/activity/"><div class="col-md-1 header-item">活动</div></a>
        <div class="col-md-1 header-item">联系我们</div>
        <div class="col-md-1 header-item">搜索</div>
        <div class="col-md-1"></div>
    </div>


    <div class="row">
    <div class="col-md-12" style="margin-bottom: 10px">
        <img src="/images/1.jpg"
             width="100%" height="150px">
    </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <img width="100%" height="400px" src="${activity.img}">
        </div>
        <div class="col-md-6">
            <div><h3> ${activity.theme} </h3></div>

            <c:if test="${activity.type  == '捐款'  }" >
                <div>目标金额： ${activity.intentmoney} </div>
                <div>已筹款： ${activity.money} </div>
            </c:if>


            <c:if test="${activity.type  == '志愿者'  }" >
                <div>内容： ${activity.content}  </div>
                <div>目标人数：${activity.peoplenum} 人</div>
                <div>地点： ${activity.address}  </div>
            </c:if>

            <c:if test="${activity.type  == '捐物'  }" >
                <div>内容： ${activity.content}  </div>
                <div>目标人数： ${activity.peoplenum} 人 </div>
                <div>地点： ${activity.address}  </div>
            </c:if>


            <div> 组织者：${user.name} <br>
                联系方式：${user.phone}<br>
                邮箱：${user.email}<br>
                目前状况：
                <table class="table">
                    <tr>
                        <td>类型</td>
                        <td>内容</td>
                        <td>状态</td>
                    </tr>
                    <c:forEach  items="${getDonates}" var="item">
                        <tr>
                            <td>${item.type}
                                <c:choose>
                                    <c:when test="${item.type  == 1  }">
                                        物品捐赠
                                    </c:when>
                                    <c:when test="${item.type  == 2  }">
                                        现金捐赠
                                    </c:when>
                                    <c:when test="${item.type  == 3  }">
                                        志愿者
                                    </c:when>
                                </c:choose>

                            </td>
                            <td>${item.content}</td>
                            <td>
                                <c:choose>

                                    <c:when test="${ empty item.status  }">
                                        已确认
                                    </c:when>

                                    <c:when test="${item.status  == 0  }">
                                        已收款
                                    </c:when>

                                    <c:when test="${item.status  == 1  }">
                                        已发放
                                    </c:when>
                                </c:choose>


                            </td>
                        </tr>
                    </c:forEach>
                </table>


            </div>

            <div><a class="btn btn-default join">参与</a></div>

        </div>

    </div>

    <c:if test="${ not empty donates}">
        <div class="row">
            <table class="table">
                <tr>
                    <td>类型</td>
                    <td>内容</td>
                    <td>状态</td>
                </tr>
                <c:forEach  items="${donates}" var="item">
                    <tr>
                        <td>${item.type}
                            <c:choose>
                                <c:when test="${item.type  == 1  }">
                                    物品捐赠
                                </c:when>
                                <c:when test="${item.type  == 2  }">
                                    现金捐赠
                                </c:when>
                                <c:when test="${item.type  == 3  }">
                                    志愿者
                                </c:when>
                            </c:choose>

                        </td>
                        <td>${item.content}</td>
                        <td>
                            <c:choose>

                                <c:when test="${ empty item.status  }">
                                    已确认
                                </c:when>

                                <c:when test="${item.status  == 0  }">
                                    已收款
                                </c:when>

                                <c:when test="${item.status  == 1  }">
                                    已发放
                                </c:when>
                            </c:choose>


                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </c:if>




</div>


<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="/activity/donateMoney" method="get">
            <input type="hidden" name="actID" value="${activity.id}">
            <input type="hidden" name="phone" value="${user.phone}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">参与</h4>
            </div>
            <div class="modal-body">


                <div class="form-group">
                    <label>参与方式</label>
                    <select name="method" class="form-control" onchange="change()" id="type">
                        <option value="1">捐物</option>
                        <option value="2">捐钱</option>
                        <option value="3">成为志愿者</option>
                    </select>
                </div>


                <div class="form-group">
                    <label>捐款数量或是捐赠物品</label>
                    <input id="content" class="form-control" name="content" placeholder="捐赠内容"></div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" id="sub">提交更改</button>
            </div>


        </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal -->
</div>

</body>
</html>
<script>
    $(".join").click(function () {
        $("#myModal").modal();
    });

    $("#sub").click(function () {
        var args=$("form").serialize()
        $.get("/activity/donate",args,function (res) {
            if (res == 200)
                window.location.reload();
            if (res == 301)
                window.location= "/user";


        })
    });

    function change() {
        var type=$("#type").val();
        if (type == 3){
            $("#content").attr("disabled",true);
        }else {
            $("input").removeAttr("disabled");
        }
    }

</script>