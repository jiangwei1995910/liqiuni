<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>个人中心</title>
    <link href="/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="/css/commen.css" type="text/css" rel="stylesheet"/>
    <link href="/css/userHome.css" type="text/css" rel="stylesheet"/>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/vue.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row header">
        <a href="/">
            <div class="col-md-1 header-item">首页</div>
        </a>
        <a href="/activity/"><div class="col-md-1 header-item">活动</div></a>
        <div class="col-md-1 header-item">联系我们</div>
        <div class="col-md-1 header-item">搜索</div>
        <div class="col-md-1"></div>
        <div class="col-md-1 header-item float-right">
            <a  href="/user/loginout" >退出</a>
        </div>
        <a href="/user/home">
            <div class="col-md-2 float-right header-item">欢迎您，${user.phone}

            </div>
        </a>



    </div>

    <div class="row">
        <div class="col-md-12">
           <label> 个人信息</label>

            <form action="/user/update" method="post" id="userInfo">
                <table class="table table-bordered" id="info">
                    <tr>
                        <td>用户ID：${user.id} <input type="hidden" value="${user.id}" name="id"></td>
                        <td > <span @click="c" class="base">姓名：{{name}}</span>
                            <span class="input"><input @change="changes" name="name" v-model="name" placeholder="姓名"> </span>
                        </td>
                        <td ><span @click="c" class="base">年龄：{{age}}
                        </span><span class="input"> <input @change="changes" name="age" v-model="age" placeholder="年龄">  </span>
                        </td>
                        <td ><span @click="c" class="base">性别：{{gender}}</span>
                            <span class="input"> <input @change="changes" v-model="gender" name="gender" placeholder="性别">  </span>
                        </td>

                    </tr>
                    <tr>
                        <td ><span @click="c" class="base">电话：{{phone}}</span>
                            <span class="input"><input @change="changes" name="phone" v-model="phone" placeholder="电话"></span>
                        </td>
                        <td ><span @click="c" class="base">邮箱：{{email}}</span>
                            <span class="input"><input @change="changes" name="email" v-model="email" placeholder="邮箱"></span>
                        </td>
                        <td ><span @click="c" class="base">地址：{{address}}</span>
                            <span class="input"><input @change="changes" v-model="address" name="address" placeholder="地址"></span></td>
                        <td></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="myproject">
                <label>我的项目</label>
                <a href="/activity/new" class="btn btn-default float-right"><span class="glyphicon glyphicon-plus"></span></a>
                <table class="table table-bordered">
                    <tr>
                        <td>类型</td>
                        <td>主题</td>
                        <td>描述</td>
                        <td>地点</td>
                        <td>时间</td>
                        <td>目标金额</td>
                        <td>善款金额</td>
                        <td>招募人数</td>
                        <td>状态</td>
                    </tr>
                    <c:forEach var="item"   items="${activities}"    >

                        <tr>
                            <td>${item.type}</td>
                            <td>${item.theme}</td>
                            <td>${item.desc}</td>
                            <td>${item.address}</td>
                            <td>${item.starttimes} --${item.endtimes} </td>
                            <td>${item.intentmoney}</td>
                            <td>${item.money}</td>
                            <td>${item.peoplenum}</td>
                            <td>
                            <c:choose>
                                <c:when test="${item.status  == 0  }">
                                    待审核
                                </c:when>

                                <c:when test="${item.status  == 1  }">
                                    审核完成
                                </c:when>

                                <c:when test="${item.status  == 2  }">
                                    进行中
                                </c:when>

                                <c:when test="${item.status  >=3  }">
                                    结束
                                </c:when>

                            </c:choose>

                            </td>

                        </tr>

                    </c:forEach>



                </table>

            </div>


        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            参加公益次数<br>
            ${count}
        </div>
        <div class="col-md-4">
            捐款总额<br>
            ${total}
        </div>

    </div>

</div>
</body>
</html>

<script>
    var v = new Vue({
        el: "#info",
        data: {
            id:'${user.id}',
            name: '${user.name}',
            age: '${user.age}',
            gender: '${user.gender}',
            phone: '${user.phone}',
            email: '${user.email}',
            address: '${user.address}'
        },
        methods: {
            c: function (e) {
                $(".base").show();
                $(".input").hide();
                $(e.target).hide();
                $(e.target).next().show();
            },
            changes:function () {
                $.post("/user/update",{ id:this.id,  name:this.name,age:this.age,gender:this.gender,phone:this.phone,email:this.email,address:this.address  },function (res) {

                },'json');
            }
        }
    });




</script>